package com.zhpr.portal.driver.transceiver.impl;

import com.zhpr.portal.driver.transceiver.IDataTransceiver;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetSocketAddress;

public class TcpSocketTransceiver implements IDataTransceiver {
    private final String IO_FILTER_NAME = "codec";
    private IoAcceptor ioAcceptor = null;
    private int port;
    private int bufferSize;
    private int idleTime;

    //implements IDataTransceiver
    @Override
    public void Send(){
    }
    @Override
    public boolean Open(IoHandler handler, IoFilter filter) throws IOException {
        if(null == this.ioAcceptor) {
            this.ioAcceptor = new NioSocketAcceptor();
        }
        this.ioAcceptor.getSessionConfig().setReadBufferSize(this.getBufferSize());
        this.ioAcceptor.getFilterChain().addLast(IO_FILTER_NAME, filter);
        this.ioAcceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, this.getIdleTime());
        this.ioAcceptor.setCloseOnDeactivation(false);
        this.ioAcceptor.setHandler(handler);
        this.ioAcceptor.bind(new InetSocketAddress(this.getPort()));
        return true;
    }
    @Override
    public boolean Open(){
        return true;
    }
    @Override
    public boolean Close(){
        return true;
    }
    @Override
    public boolean Connect(){
        return true;
    }
    @Override
    public boolean Disconnect(){
        return true;
    }
    @Override
    public void Publish(String data){

    }
    @Override
    public void Publish(String topic,String data){

    }
    @Override
    public void Subscribe(String topic){

    }

    // getter and setter
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public int getIdleTime() {
        return idleTime;
    }

    public void setIdleTime(int idleTime) {
        this.idleTime = idleTime;
    }
}
