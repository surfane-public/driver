package com.zhpr.portal.driver.transceiver.impl;

import com.zhpr.portal.driver.transceiver.IDataTransceiver;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.service.IoHandler;

import java.io.IOException;

public class MqttTransceiver implements IDataTransceiver{
    //implements IDataTransceiver
    @Override
    public void Send(){
    }
    @Override
    public boolean Open(IoHandler handler, IoFilter filter) throws IOException {
        return true;
    }
    @Override
    public boolean Open(){
        return true;
    }
    @Override
    public boolean Close(){
        return true;
    }
    @Override
    public boolean Connect(){
        return true;
    }
    @Override
    public boolean Disconnect(){
        return true;
    }
    @Override
    public void Publish(String data){

    }
    @Override
    public void Publish(String topic,String data){

    }
    @Override
    public void Subscribe(String topic){

    }
}
