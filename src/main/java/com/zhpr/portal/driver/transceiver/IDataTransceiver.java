package com.zhpr.portal.driver.transceiver;

import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.service.IoHandler;

import java.io.IOException;

public interface IDataTransceiver {
    public boolean Open();
    public boolean Open(IoHandler handler, IoFilter filter) throws IOException;
    public boolean Close();
    public boolean Connect();
    public boolean Disconnect();

    public void Send();
    public void Publish(String data);
    public void Publish(String topic,String data);
    public void Subscribe(String topic);
}
