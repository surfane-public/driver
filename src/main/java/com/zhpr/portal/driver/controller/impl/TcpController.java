package com.zhpr.portal.driver.controller.impl;

import com.zhpr.portal.driver.adapter.IDataAdapter;
import com.zhpr.portal.driver.controller.IDataController;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.springframework.beans.factory.annotation.Autowired;

public class TcpController implements IDataController {
    @Autowired
    private IDataAdapter adapter;

    //implements IoHandler
    @Override
    public void messageReceived(IoSession session, Object message)
    {
        IoBuffer ioBuffer = (IoBuffer) message;
//        int curPos = ioBuffer.position();
//        ioBuffer.position(0);
//        ioBuffer.position(curPos);
//        byte[] bytes = new byte[ioBuffer.limit()];
//        for (int i = 0; i < ioBuffer.limit(); i++)
//        {
//            bytes[i] = ioBuffer.array()[i];
//        }
        byte [] bytes = new byte[ioBuffer.remaining()];
        ioBuffer.get(bytes);
        if(this.adapter.CheckFormat(bytes.toString())) {
            this.adapter.Adapt(session.getRemoteAddress().toString(), bytes.toString());
        }
    }


    @Override
    public void exceptionCaught(IoSession arg0, Throwable arg1)
    {
    }

    @Override
    public void inputClosed(IoSession arg0) throws Exception
    {
//        this.connectDrop(arg0);
    }

    @Override
    public void messageSent(IoSession arg0, Object arg1) throws Exception
    {
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception
    {
//        this.connectDrop(session);
    }

    @Override
    public void sessionCreated(IoSession session) throws Exception
    {
    }

    @Override
    public void sessionIdle(IoSession session, IdleStatus arg1) throws Exception
    {
//        if (null != session && null != Service.SESSION_INFO.get(session))
//        {
//            Service.SESSION_INFO.get(session).setDate(new Date());
//        }
    }

    @Override
    public void sessionOpened(IoSession arg0) throws Exception
    {
    }
}
