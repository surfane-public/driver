package com.zhpr.portal.driver.controller;

import com.zhpr.portal.driver.controller.impl.TcpController;
import com.zhpr.portal.driver.transceiver.impl.TcpSocketTransceiver;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.service.IoHandler;
import org.springframework.beans.factory.annotation.Value;

@org.springframework.stereotype.Service
public class Service {
    private TcpSocketTransceiver tcpSocketTransceiver;
    private IoHandler ioHandler;
    private IoFilter ioFilter;

    @Value("${tcpsocket.port}")
    private int port;
    @Value("${tcpsocket.bufferSize}")
    private int bufferSize;
    @Value("${tcpsocket.idleTime}")
    private int idleTime;

    public void InitTcpSocket(){
        this.tcpSocketTransceiver = new TcpSocketTransceiver();
        this.ioHandler = new TcpController();
        this.ioFilter = new
        this.tcpSocketTransceiver.setPort(this.port);
        this.tcpSocketTransceiver.setIdleTime(this.idleTime);
        this.tcpSocketTransceiver.setBufferSize(this.bufferSize);
        this.ioHandler = new TcpController();

    }

    public void OpenTcpSocket(){

    }

}
