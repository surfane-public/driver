package com.zhpr.portal.driver.adapter.impl;

import com.zhpr.portal.driver.adapter.IDataAdapter;
import org.apache.mina.core.session.IoSession;

public class JsonAdapter implements IDataAdapter {

    @Override
    public boolean CheckFormat(byte[] data){
        return true;
    }
    @Override
    public boolean CheckFormat(String data){
        return true;
    }
    @Override
    public void Adapt(Long remote, String data){

    }
    @Override
    public void Adapt(Long remote, byte[] data){

    }
    @Override
    public void Adapt(String remote, String data){

    }
    @Override
    public void Adapt(String remote, byte[] data){

    }
    @Override
    public void Adapt(IoSession remote, String data){

    }
    @Override
    public void Adapt(IoSession remote, byte[] data){

    }
}
