package com.zhpr.portal.driver.adapter;

import org.apache.mina.core.session.IoSession;

public interface IDataAdapter {
    public boolean CheckFormat(byte[] data);
    public boolean CheckFormat(String data);
    public void Adapt(Long remote, String data);
    public void Adapt(Long remote, byte[] data);
    public void Adapt(String remote, String data);
    public void Adapt(String remote, byte[] data);
    public void Adapt(IoSession remote, String data);
    public void Adapt(IoSession remote, byte[] data);
}
